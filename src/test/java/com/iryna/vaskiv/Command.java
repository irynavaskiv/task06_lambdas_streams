package com.iryna.vaskiv;

@FunctionalInterface

public interface Command {
       public void execute(String argument);
    }

